<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="Article", uniqueConstraints = {@ORM\UniqueConstraint(name="no_repeated_articleTitle", columns={"article_title"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ArticleRepository")
 */
class Article
{
	const MAXIMUM_ARTICLES_IN_HOMEPAGE=6;
	
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="article_title", type="string", length=255)
     */
    private $articleTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="article_description", type="text", length=2000)
     */
    private $articleDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="article_content", type="text")
     */
    private $articleContent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="article_creation_date", type="date")
     */
    private $articleCreationDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="article_published", type="boolean")
     */
    private $articlePublished;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="relatedArticle", cascade={"remove"})
     * @ORM\OrderBy({"commentCreationDate" = "ASC"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag", mappedBy="articles")
     * @ORM\OrderBy({"tag_name" = "ASC"})
     */
    private $tags; 

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleTitle
     *
     * @param string $articleTitle
     *
     * @return Article
     */
    public function setArticleTitle($articleTitle)
    {
        $this->articleTitle = $articleTitle;

        return $this;
    }

    /**
     * Get articleTitle
     *
     * @return string
     */
    public function getArticleTitle()
    {
        return $this->articleTitle;
    }

    /**
     * Set articleDescription
     *
     * @param string $articleDescription
     *
     * @return Article
     */
    public function setArticleDescription($articleDescription)
    {
        $this->articleDescription = $articleDescription;

        return $this;
    }

    /**
     * Get articleDescription
     *
     * @return string
     */
    public function getArticleDescription()
    {
        return $this->articleDescription;
    }

    /**
     * Set articleContent
     *
     * @param string $articleContent
     *
     * @return Article
     */
    public function setArticleContent($articleContent)
    {
        $this->articleContent = $articleContent;

        return $this;
    }

    /**
     * Get articleContent
     *
     * @return string
     */
    public function getArticleContent()
    {
        return $this->articleContent;
    }

    /**
     * Set articleCreationDate
     *
     * @param \DateTime $articleCreationDate
     *
     * @return Article
     */
    public function setArticleCreationDate($articleCreationDate)
    {
        $this->articleCreationDate = $articleCreationDate;

        return $this;
    }

    /**
     * Get articleCreationDate
     *
     * @return \DateTime
     */
    public function getArticleCreationDate()
    {
        return $this->articleCreationDate;
    }

    /**
     * Set articlePublished
     *
     * @param boolean $articlePublished
     *
     * @return Article
     */
    public function setArticlePublished($articlePublished)
    {
        $this->articlePublished = $articlePublished;

        return $this;
    }

    /**
     * Get articlePublished
     *
     * @return bool
     */
    public function getArticlePublished()
    {
        return $this->articlePublished;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Article
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return Article
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
}
